<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>
    <style>

        .outer-box {
            padding: 60px;
            width: 40%;
            border: 2px solid #648bae;
            margin: 10% auto 0 auto;
            display: flex;
        }

        .register-form {
            width: 100%;
            margin-top: 10px;
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
        }

        .register-form-item {
            width: 45%;
            margin-bottom: 10px;
            display: flex;
            align-items: center;
        }

        label {
            margin-right: 10px;
            flex-shrink: 0;
            display: block;
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #99CC00;
            border: 2px solid #648bae;
            font-size: 16px;
            text-align: center;
            width: 20%;
        }

        label#address {
            display: block;
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #99CC00;
            border: 2px solid #648bae;
            font-size: 16px;
            text-align: center;
            width: 60%;
            
        }
        

        .required {
            color: #FF0000;
            margin-left: 5px;
        }

        input[type="text"]#username
        {
            width: 60%;
            padding: 10px;
            border: 1px solid #ccc;
            margin-bottom: 20px;
            box-sizing: border-box;
            border: 2px solid #648bae;
            margin-right: 80px;
            flex-grow: 1;
        }

        input[type="text"]#dob {
            width: 40%;
            padding: 10px;
            margin-bottom: 20px;
            box-sizing: border-box;
            border: 2px solid #648bae;
            margin-right: 20px;
            flex-grow: 1;
        }

        select#majorelect {
            width: 40%;
            padding: 10px;
            margin-bottom: 20px;
            box-sizing: border-box;
            border: 2px solid #648bae;
            margin-right: 20px;
            flex-grow: 1;
        }

        input[type="text"]#address 
        {
            width: 60%;
            padding: 10px;
            margin-bottom: 20px;
            box-sizing: border-box;
            border: 2px solid #648bae;
            margin-right: 80px;
            flex-grow: 1;
        }



        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 30%;
            padding: 10px;
            border: 2px solid #648bae;
            background-color: #99CC00;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }

        #error-message {
            width: 50%;
            color: red;
        }
    </style>
</head>


<body>
    <div class="outer-box">
        <form method="POST" onsubmit="return validateForm()">
            <div class="register-form">
                </div><div id="error-message"></div> 
            <div>

            <div class="register-form">
                <label for="username">Họ và tên<span class="required">*</span></label>
                <input type="text" id="username" name="username" size ="32">
            <div>

            <div class="register-form" >
                <label class="gender">Giới tính<span class="required">*</span></label>
                
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ');
                    for ($i = 0; $i < count($genders); $i++) {
                        $gender = $genders[$i];
                        echo '<input type="radio" id="gender_'.$i.'" name="gender" value="'.$gender.'">'.$gender;
                        
                    }
                    ?>
            </div>
                    
            <div class="register-form">
                <label for="major">Phân khoa<span class="required">*</span></label>
                <select id="majorelect" name="major">
                        <option value="--Chọn phân khoa--">--Chọn phân khoa--</option>
                        <?php
                        $majors = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

                        foreach ($majors as $key => $value) {
                            echo "<option value='$key'>$value</option>";
                        }
                        ?>
                </select><br>
            <div>

            <div class="register-form">
                <label for="dob">Ngày sinh<span class="required">*</span></label>
                <input type="text" id="dob" name="dob" size="32">
            <div>
                
            <div class="register-form">
                <label for="address">Địa chỉ</label>
                <input type="text" id="address" name="address" size="32">
            </div>
                        
            <button type="submit" id="register-button">Đăng ký</button>
        </form>
    </div>

    <script>
        function validateForm() {
            var errorMessage = document.getElementById("error-message");

            var username = document.getElementById("username").value;
            var major = document.getElementById("majorelect").value;
            var dob = document.getElementById("dob").value;

            errorMessage.innerHTML = ""; // Clear previous error message

            if (username.trim() === "") {
                errorMessage.innerHTML += "Hãy nhập tên.<br>";
            }

            if (major === "--Chọn phân khoa--") {
                errorMessage.innerHTML += "Hãy chọn phân khoa.<br>";
            }

            if (dob.trim() === "") {
                errorMessage.innerHTML += "Hãy nhập ngày sinh.<br>";
            } else {
                var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
                if (!dateRegex.test(dob)) {
                    errorMessage.innerHTML += "Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.<br>";
                }
            }

            return false; // Prevent form submission
        }
    </script>
</body>

</html>
